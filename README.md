# README #

API project to finetune Word2Vec model word2vec-google-news-300

### How do I get set up? ###

* add settings_local.py to iris/iris/iris. It should contain the following parameters:

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'iris',
        'USER': 'iris',
        'PASSWORD': '<password>',
        'HOST': 'localhost',
        'PORT': '',
    }
}

DATASET_PATH = '/path/to/dataset.json'
REF_VOCAB_PATH = '/path/to/ref_vocab.json'
REF_VOCAB_MATRIX_PATH = '/path/to/ref_vocab_matrix.pkl'
```

* create said db and run migrations

`python manage.py migrate`

* This is a Django project. To test you can run

`python manage.py runserver` 

### API ###

`/hyperparameters`
* POST
* Body - JSON object of type HyperParameters:
```
{"epochs":<int>,"start_alpha":<float>,"end_alpha":<float>}
```
* Description - creates new hyper parameters configuration and return it with its id.
The id could be later used to start training
* Response - JSON object of type HyperParameters with id:
```
{"id":<int>,epochs":<int>,"start_alpha":<float>,"end_alpha":<float>}
```

`/range_hyperparameters`
* POST
* Body - JSON object of type HyperParameters that could accept ranges instead of values (or values):
```
{"epochs":[<int>-<bigger_int>],"start_alpha":[<float>-<biger_float>,<step_float>],"end_alpha":[<float>-<biger_float>,<step_float>]}
```
* Description - creates new set hyper parameters configuration and return them with their ids.
Creates all possible combinations form the given ranges. The ids could be later used to start training
* Response - JSON object of type array of HyperParameters with id:
```
[{"id":<int>,epochs":<int>,"start_alpha":<float>,"end_alpha":<float>}...]
```

`/train/<int:hyper_parameters_id>/`
* GET
* Path parameter - the id of the already created HyperParameters
* Description - triggers finetuning of the model without waiting for it to end.
* Response - HTTP status 200

`result/<int:hyper_parameters_id>/`
* GET
* Path parameter - the id of the already created HyperParameters
* Description - Finds what is the status of the training with said HyperParameters
* Response - If the training is finished it returns ModelPerformance JSON object:
```
{"id": 3, "code_version": 1, "data_version": 1, "pip_distance": 0.255795532053126, "training_time_seconds": 38.0689849853516}
```