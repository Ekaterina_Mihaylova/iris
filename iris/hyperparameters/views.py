from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import HyperParameters, ModelPerformance
from .train import enrich_model
from .serializers import HyperParametersSerializer, ModelPerformanceSerializer, HyperParametersRangeSerializer
import threading


def run_training(request, hyper_parameters_id):
    if request.method == 'GET':
        hyper_parameters = HyperParameters.objects.get(id=hyper_parameters_id)
        enriching = threading.Thread(target=enrich_model, args=(hyper_parameters,))
        enriching.start()
        return HttpResponse(status=200)


@csrf_exempt
def set_range_hyper_parameters(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        range_serializer = HyperParametersRangeSerializer(data=data)
        if range_serializer.is_valid():
            hyper_parameters_array = range_serializer.get_hyper_parameters_array()
            for hyper_parameters in hyper_parameters_array:
                hyper_parameters.save()
            serializer = HyperParametersSerializer(hyper_parameters_array, many=True)
            return JsonResponse(serializer.data, safe=False, status=201)
        return JsonResponse(range_serializer.errors, safe=False, status=400)


@csrf_exempt
def set_hyper_parameters(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = HyperParametersSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


def get_result(request, hyper_parameters_id):
    if request.method == 'GET':
        if not HyperParameters.objects.filter(id=hyper_parameters_id).exists():
            return HttpResponse("Hyper parameters not found", status=404)
        hyper_parameters = HyperParameters.objects.get(id=hyper_parameters_id)
        if not ModelPerformance.objects.filter(hyper_parameters=hyper_parameters).exists():
            return HttpResponse("Model not finished training", status=202)
        model_performance = ModelPerformance.objects.get(hyper_parameters=hyper_parameters)
        serializer = ModelPerformanceSerializer(model_performance)
        return JsonResponse(serializer.data, status=200)
