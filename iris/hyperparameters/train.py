from django.conf import settings
import json
import time
import pickle
import gensim.downloader as api
from gensim.utils import simple_preprocess
from gensim.summarization.textcleaner import get_sentences
from .models import HyperParameters, ModelPerformance
from .metrics import calculate_pip_distance

CODE_VERSION = 1
DATA_VERSION = 1


def get_tokenized_sentences(text):
    sentences = []
    for sentence in get_sentences(text):
        sentences.append(simple_preprocess(sentence, deacc=True))
    return sentences


def load_sentences():
    sentences = []
    with open(settings.DATASET_PATH) as json_file:
        data = json.load(json_file)
        for article in data:
            sentences.extend(get_tokenized_sentences(article['title']))
            sentences.extend(get_tokenized_sentences(article['description']))
    return sentences


def enrich_model(hyper_parameters: HyperParameters):
    start_time = time.time()
    model = api.load("word2vec-google-news-300")
    # model = api.load("__testing_word2vec-matrix-synopsis")
    sentences = load_sentences()
    model.build_vocab(sentences, update=True)
    model.train(sentences, total_examples=model.corpus_count, epochs=hyper_parameters.epochs,
                start_alpha=hyper_parameters.start_alpha, end_alpha=hyper_parameters.end_alpha)
    reference_words_list = []
    with open(settings.REF_VOCAB_PATH) as json_file:
        reference_words_list = json.load(json_file)
    reference_word_model_matrix = pickle.load(open(settings.REF_VOCAB_MATRIX_PATH, "rb"))
    pip_distance = calculate_pip_distance(reference_words_list, reference_word_model_matrix, model)
    model_performance = ModelPerformance()
    model_performance.pip_distance = pip_distance
    model_performance.hyper_parameters = hyper_parameters
    model_performance.code_version = CODE_VERSION
    model_performance.data_version = DATA_VERSION
    model_performance.training_time_seconds = time.time() - start_time
    model_performance.save()
