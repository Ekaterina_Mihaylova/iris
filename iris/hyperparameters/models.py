from django.db import models


class HyperParameters(models.Model):
    epochs = models.IntegerField()
    start_alpha = models.FloatField()
    end_alpha = models.FloatField()

    def __str__(self):
        return "epochs : %d, start_alpha : %d, end_alpha : %d" % (self.epochs, self.start_alpha, self.end_alpha)


class ModelPerformance(models.Model):
    hyper_parameters = models.ForeignKey(HyperParameters, on_delete=models.CASCADE)
    code_version = models.IntegerField()
    data_version = models.IntegerField()
    pip_distance = models.FloatField()
    training_time_seconds = models.FloatField()

