from rest_framework import serializers
import numpy
from .models import HyperParameters, ModelPerformance


class HyperParametersSerializer(serializers.ModelSerializer):
    class Meta:
        model = HyperParameters
        fields = ['id', 'epochs', 'start_alpha', 'end_alpha']


class ModelPerformanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelPerformance
        fields = ['id', 'code_version', 'data_version', 'pip_distance', 'training_time_seconds']


class HyperParametersRangeSerializer:
    MAX_HYPER_PARAMETERS = 100

    @staticmethod
    def is_float(s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    @staticmethod
    def is_valid_int_range(start_end_strings):
        if len(start_end_strings) != 2:
            return False
        if not start_end_strings[0].startswith('[') or not start_end_strings[1].endswith(']'):
            return False
        start = start_end_strings[0][1:].strip()
        end = start_end_strings[1][0:-1].strip()
        if not start.isdecimal() or not end.isdecimal():
            return False
        if int(start) > int(end):
            return False
        return True

    @staticmethod
    def is_valid_float_range(range_string):
        range_and_step = range_string.split(',')
        if len(range_and_step) != 2:
            return False

        start_end = range_and_step[0].split('-')
        if len(start_end) != 2:
            return False

        if not start_end[0].startswith('[') or not range_and_step[1].endswith(']'):
            return False
        start = start_end[0][1:].strip()
        end = start_end[1].strip()
        step = range_and_step[1][0:-1].strip()
        if not HyperParametersRangeSerializer.is_float(start)\
                or not HyperParametersRangeSerializer.is_float(end)\
                or not HyperParametersRangeSerializer.is_float(step):
            return False
        if float(start) > float(end):
            return False
        if (float(end) - float(start)) % float(step) != 0:
            return False
        return True

    @staticmethod
    def get_float_range_parameters(range_string):
        range_and_step = range_string.split(',')
        start_end = range_and_step[0].split('-')
        start = start_end[0][1:].strip()
        end = start_end[1].strip()
        step = range_and_step[1][0:-1].strip()
        return [float(start), float(end), float(step)]

    def __init__(self, data):
        self.all_epochs = []
        self.all_start_alpha = []
        self.all_end_alpha = []
        self.errors = []

        range_epochs = data.get("epochs").strip()
        range_start_alpha = data.get("start_alpha").strip()
        range_end_alpha = data.get("end_alpha").strip()

        if '-' in range_epochs:
            start_end_epochs = range_epochs.split('-')
            if not HyperParametersRangeSerializer.is_valid_int_range(start_end_epochs):
                self.errors.append("epochs range is not valid. It should be of the type [int-int] and increasing. "
                                   "Instead it is " + range_epochs)
                return
            start_epochs = int(start_end_epochs[0][1:].strip())
            end_epochs = int(start_end_epochs[1][0:-1].strip()) + 1
            for epochs in range(start_epochs, end_epochs):
                self.all_epochs.append(epochs)
        elif range_epochs.isdecimal():
            self.all_epochs.append(int(range_epochs))
        else:
            self.errors.append("epochs not given, not a decimal or not a range. "
                               "It should be of the type 'int' or '[int-bigger_int]'. Instead it is "
                               + range_epochs)
            return

        if '-' in range_start_alpha:
            if not HyperParametersRangeSerializer.is_valid_float_range(range_start_alpha):
                self.errors.append("start_alpha range is not valid. It should be of the type [float-bigger_float,step_float]. "
                                   "The step should be able to take you from the float to the bigger_float. Instead the range is "
                                   + range_start_alpha)
                return
            start, end, step = HyperParametersRangeSerializer.get_float_range_parameters(range_start_alpha)
            for start_alpha in numpy.linspace(start, end, num=int((end-start)/step)+1):
                self.all_start_alpha.append(start_alpha)
        elif HyperParametersRangeSerializer.is_float(range_start_alpha):
            self.all_start_alpha.append(float(range_start_alpha))
        else:
            self.errors.append("start_alpha not given, not a decimal or not a range. "
                               "It should be of the type 'float' or '[float-float,float]'. Instead it is "
                               + range_start_alpha)
            return

        if '-' in range_end_alpha:
            if not HyperParametersRangeSerializer.is_valid_float_range(range_end_alpha):
                self.errors.append("end_alpha range is not valid. It should be of the type [float-bigger_float,step_float]. "
                                   "The step should be able to take you from the float to the bigger_float. Instead the range is "
                                   + range_end_alpha)
                return
            start, end, step = HyperParametersRangeSerializer.get_float_range_parameters(range_end_alpha)
            for end_alpha in numpy.linspace(start, end, num=int((end-start)/step)+1):
                self.all_end_alpha.append(end_alpha)
        elif HyperParametersRangeSerializer.is_float(range_end_alpha):
            self.all_end_alpha.append(float(range_end_alpha))
        else:
            self.errors.append("start_alpha not given, not a decimal or not a range. "
                               "It should be of the type 'float' or '[float-float,float]'. Instead it is "
                               + range_end_alpha)
            return

        if len(self.all_start_alpha) * len(self.all_end_alpha) * len(self.all_epochs) > self.MAX_HYPER_PARAMETERS:
            self.errors.append("Too many combinations of hyper parameters. Maximum acceptable is " + self.MAX_HYPER_PARAMETERS)

    def get_hyper_parameters_array(self):
        all_hyper_parameters = []
        for epochs in self.all_epochs:
            for start_alpha in self.all_start_alpha:
                for end_alpha in self.all_end_alpha:
                    hyper_parameters = HyperParameters()
                    hyper_parameters.start_alpha = start_alpha
                    hyper_parameters.end_alpha = end_alpha
                    hyper_parameters.epochs = epochs
                    all_hyper_parameters.append(hyper_parameters)
        return all_hyper_parameters

    def is_valid(self):
        return len(self.errors) == 0